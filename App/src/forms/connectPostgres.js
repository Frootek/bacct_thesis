const electron = require('electron')
const remote = electron.remote
const ipc=electron.ipcRenderer
const dialog = require('electron')
const {Client} = require ("../../node_modules/pg")
const path = require('path')


const cancelBtn = document.getElementById("cancel-btn")

cancelBtn.addEventListener('click', function (event) {
    var window = remote.getCurrentWindow();
    window.close();
})

const connectBtn = document.getElementById("connect")

connectBtn.addEventListener('click', function(event) {

    try{
        //un - comment this part after development
        const user  = document.getElementById("user-name").value
        const host  = document.getElementById("host-name").value
        const dB  = document.getElementById("database-name").value
        const password  = document.getElementById("db-password").value
        const port  = document.getElementById("port-number").value

        const client = new Client({
            user: user,
            host: host,
            database: dB,
            password: password,
            port: port,
        })


        client.connect().then(() => {
            
            ipc.send('connection-success', client,password)
            var window = remote.getCurrentWindow();
            window.close();
        }).catch(e => 

            alert("Not able to connect to database, check your credentials and try again")
        )
    }catch(err){
       console.log(err)
    }
})

