var echarts = require ("echarts")
var path = require("path")
const db =  require (path.join(__dirname,"../mySQL/db/index.js"))

const currentClient = JSON.parse(window.localStorage.getItem("client"))


var selectDropdown_1 = document.getElementById("sel1")
var selectDropdown_2 = document.getElementById("sel2")
var vis  = document.getElementById("visualize")

var bar = document.getElementById("bar-chart")
var pie = document.getElementById("pie-chart")

var showTen = document.getElementById("show-ten");
var showAll = document.getElementById("show-all");

var table
var column 
var logOn = false;

var control = {
    scale : logOn,
    colorScheme : "light",
    chart : "bar-chart",
    show : "show-ten"
}

var color_light ={
    background:"#ffff",
    axis : "black",
    item : "#C1232B"
}
var color_dark ={
    background:"#404a59",
    axis : "white",
    item : "#dd4444"
}

bar.addEventListener("click",function (event) {
    if(control.chart != "bar-chart"){
        pie.classList.toggle("btn-primary")
        bar.classList.toggle("btn-primary")
        control.chart = "bar-chart"
        visualize()
    }
})

pie.addEventListener("click",function (event) {
    if(control.chart != "pie-chart"){
        pie.classList.toggle("btn-primary")
        bar.classList.toggle("btn-primary")
        control.chart = "pie-chart"
        visualize()
    }
})

showTen.addEventListener("click",function (event) {
    if(control.show != "show-ten"){
        showTen.classList.toggle("btn-primary")
        showAll.classList.toggle("btn-primary")
        control.show = "show-ten"
        visualize()
    }
})

showAll.addEventListener("click",function (event) {
    if(control.show != "show-all"){
        showTen.classList.toggle("btn-primary")
        showAll.classList.toggle("btn-primary")
        control.show = "show-all"
        visualize()
    }
})


vis.addEventListener("click" ,function (event){
    visualize()
})

selectDropdown_1.addEventListener("change", async function(event){

    table = document.getElementById("sel1").value
    selectDropdown_2.options.length=0

    var tableColumns =  `SELECT COLUMN_NAME AS "column_name"
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_SCHEMA = '${currentClient.database}'
       AND table_name   = '${table}';`

    var columns = await db.query(tableColumns)

    for (var i = 0; i<columns.length; ++i)
    {
        var optionElement2 = document.createElement("option")
        optionElement2.value = columns[i].column_name
        optionElement2.text = columns[i].column_name
        selectDropdown_2.add(optionElement2)      
    }

    //check if table is to big to show all data and disable show-all button

    var table_count_columns = `SELECT table_name AS "relation", table_rows AS "estimate"
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA = '${currentClient.database}'`

    var count_columns = await db.query(table_count_columns)
    var find_table = count_columns.filter(obj => {
        return obj.relation === table
      })
    if(+find_table[0].estimate > 50000){
        showAll.disabled = true
    }else{
        showAll.disabled = false
    }

})


async function main(){

    const queryTableNames =`SELECT table_name AS "table_name"
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA = '${currentClient.database}'`

    var table_names = await db.query(queryTableNames)

    for (var i = 0 ; i < table_names.length ; ++i)
    {
        var optionElement = document.createElement("option")
        optionElement.value = table_names[i].table_name
        optionElement.text = table_names[i].table_name
        selectDropdown_1.add(optionElement)

    }
}

main();


async function visualize(){

    table = selectDropdown_1.value
    column = selectDropdown_2.value
    
    var limit = ""
    if(control.show=="show-ten"){
        limit = `ORDER BY count_values desc
        LIMIT 10;`
    }
    

    var query =  `SELECT ${column} as col,COUNT(*) as count_values
        FROM ${table}
        group by ${column}
        ${limit};`


    var result = await db.query(query)

    result.sort((a,b) => +a.count_values >= +b.count_values  ? -1 : 1)

    if(control.show=="show-ten" && result.length>10)
    {
        result.length = 10;
    }

    var myChart = echarts.init(document.getElementById("myGraph"))

    var names = []
    var values = [] 

    for(var i = 0 ; i < result.length; ++i)
    {
        names.push(result[i].col)
        values.push(result[i].count_values)
    }

    var option 
    
    if(control.chart == "bar-chart")
    {
        option = {
            title: {
                x: 'center',
                text: 'Database: '+currentClient.database,
            },
            dataZoom: [
                {
                    id: 'dataZoomX',
                    type: 'slider',
                    xAxisIndex: [0],
                    filterMode: 'empty'
                },
            ],
            tooltip: {
                trigger: 'axis',
                axisPointer: {            
                    type: 'shadow'       
                }
            },
            backgroundColor : control.colorScheme=="light" ? color_light.background : color_dark.background,
            toolbox: {
                show: true,
                feature: {
                    dataView: {
                        title : "Data view",
                        show: true, 
                        readOnly: false},
                    saveAsImage: {
                        title : "Save as image",
                        show: true}
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    show: false,
                    data: names
                }
            ],
            yAxis: [
                {
                    axisLine: {
                        lineStyle:{
                            color: control.colorScheme=="light" ? color_light.axis : color_dark.axis,
                        },
                    },
                    axisLabel: {
                        color :control.colorScheme=="light" ? color_light.axis : color_dark.axis,
                    },
                    type: 'value',
                    show: true
                }
            ],
            series: [
                {
                    name: 'Row count',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: control.colorScheme=="light" ? color_light.item : color_dark.item,
                            label: {
                                show: true,
                                position: 'top',
                                formatter: '{b}\n{c}'
                            }
                        }
                    },
                    data: values,
                }
            ]
        };
    }
    if(control.chart == "pie-chart")
    {
        //first prepare data for pie chart

        var dataPie  = []
        for(var i=0 ; i<result.length ;++i)
        {
            dataPie.push({
                value: +result[i].count_values,
                name : result[i].col
            })
        }
        option = {

            title: {
                x: 'center',
                text: 'Database: ' + currentClient.database,
            },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },
            toolbox: {
                show: true,
                center: ['50%', '75%'],
                feature: {
                    dataView: {
                        title : "Data view",
                        show: true, 
                        readOnly: false},
                    saveAsImage: {
                        title : "Save as image",
                        show: true}
                }
            },
            legend: {
                type: 'scroll',
                orient: 'vertical',
                left: 10,
            },
            series: [
                {
                    name: currentClient.database,
                    type: 'pie',
                    avoidLabelOverlap: false,
                    label: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    },
                    label: {
                        show : true
                    },
                    labelLine: {
                        show: false
                        
                    },
                    data: dataPie
                }
            ]
        };
    }
    myChart.setOption(option,true)
}