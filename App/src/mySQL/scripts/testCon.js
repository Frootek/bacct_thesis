
var mysql      = require('../../../node_modules/mysql2');

var pool = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : 'Ovo je password',
  database : 'employees',
  port : "3306"
});

const promisePool = pool.promise();
const result = await promisePool.query("select * from employees limit 5" ).then(res => res[0]);

module.exports = {
  query: function (text) {
      return promisePool.query(text).then(res => res[0])
  }
}