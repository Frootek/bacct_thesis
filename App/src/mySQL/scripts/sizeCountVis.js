var echarts = require ("echarts")
var path = require("path")
const db =  require (path.join(__dirname,"../mySQL/db/index.js"))

const currentClient = JSON.parse(window.localStorage.getItem("client"))
//define buttons of control
var logOn = false
var lin = document.getElementById("lin");
var log = document.getElementById("log");

var bar = document.getElementById("bar-chart");
var pie = document.getElementById("pie-chart")

var showTen = document.getElementById("show-ten");
var showAll = document.getElementById("show-all");

var select_multiple = document.getElementById("ms")
var custom = document.getElementById("custom-pick")

var regex = document.getElementById("regex")

var control = {
    scale : logOn,
    chart : "bar-chart",
    show : "show-ten",
    custom: false
}

showTen.addEventListener("click",function (event) {
    if(control.show != "show-ten" || control.custom==true){
        showTen.classList.add("btn-primary")
        showAll.classList.remove("btn-primary")
        control.show = "show-ten"
        control.custom=false
        main()
    }
})

showAll.addEventListener("click",function (event) {
    if(control.show != "show-all" || control.custom==true){
        showTen.classList.remove("btn-primary")
        showAll.classList.add("btn-primary")
        control.show = "show-all"
        control.custom=false
        main()
    }
})

custom.addEventListener("click",function(event){
    showAll.classList.remove("btn-primary")
    showTen.classList.remove("btn-primary")
    control.custom = true;
    main()
})



bar.addEventListener("click",function (event) {
    if(control.chart != "bar-chart"){
        pie.classList.toggle("btn-primary")
        bar.classList.toggle("btn-primary")
        control.chart = "bar-chart"
        main()
    }
})

pie.addEventListener("click",function (event) {
    if(control.chart != "pie-chart"){
        pie.classList.toggle("btn-primary")
        bar.classList.toggle("btn-primary")
        control.chart = "pie-chart"
        main()
    }
})

lin.addEventListener("click",function (event) {
    if(logOn == true){
        lin.classList.toggle("btn-primary")
        log.classList.toggle("btn-primary")
        logOn = false;
        control.scale=logOn;
        if(control.chart == "bar-chart")
        {
            main()
        }
    }
})

log.addEventListener("click",function (event) {
    if(logOn == false){
        lin.classList.toggle("btn-primary")
        log.classList.toggle("btn-primary")
        logOn = true;
        control.scale=logOn;
        if(control.chart == "bar-chart")
        {
            main()
        }
    }
})

regex.addEventListener("click",async function(event){
    var name_like  = document.getElementById("regex_search").value
    name_like = name_like.toString().replace(/\*/g, "%")
   
    let queryTableSizeLike =  `SELECT table_name AS "relation",
    round(((data_length + index_length) / 1024 / 1024), 2) "total_size"
    FROM
        information_schema.TABLES
    WHERE table_name LIKE '${name_like}'
    AND TABLE_SCHEMA = '${currentClient.database}'
    ORDER BY
        (data_length + index_length)
    DESC;`



    let result = await db.query(queryTableSizeLike);

    let len = result.length
    let regex_select
    for (let i = 0 ; i < len ; i++)
    {

        regex_select = result[i].relation+ " " + result[i].total_size
        var oldValue = $('#ms').val() || []; // retrieve old value selected
        oldValue.push(regex_select); // add the new val
        $('#ms').val(oldValue).trigger('change'); // update the select and notify

    }

    showAll.classList.remove("btn-primary")
    showTen.classList.remove("btn-primary")

    control.custom = true;
    main()
})

function initialize_ms(options){
    len = options.length
    select_multiple.length=0
    for(let i = 0;i<len;++i){
        var option_element = document.createElement("option")
        option_element.value = options[i].relation+ " " + options[i].total_size
        option_element.text = options[i].relation +  "   " +  options[i].total_size
        select_multiple.add(option_element)      
    }
}


async function main(){

    var queryTableSize
    var table_size = []

    if(control.custom == false)
    {
            queryTableSize =  `SELECT
                table_name AS "relation",
                round(((data_length + index_length) / 1024 / 1024), 2) "total_size"
                FROM
                information_schema.TABLES
                WHERE TABLE_SCHEMA = '${currentClient.database}'
                AND round(((data_length + index_length) / 1024 / 1024), 2) IS NOT NULL 
                ORDER BY
                (data_length + index_length)
                DESC;`


            table_size = await db.query(queryTableSize)
            console.log(table_size)
            table_size.sort((a,b) => +a.total_size >= +b.total_size  ? -1 : 1)
            initialize_ms(table_size)
    }

    if(control.custom==true){
        values = Array.from(select_multiple.selectedOptions).map(option=>option.value)
        let len = values.length

        for(let i = 0; i < len ;++i){

            value = {
                relation : values[i].split(" ")[0],
                total_size : +values[i].split(" ")[1]
            }
            table_size.push(value)
        }
    }

    //console.log(table_size)

    var table_names = []
    var sizes  = []


    if(control.show=="show-ten" && control.custom==false && table_size.length>10)
    {
        table_size.length=10;
    }

    len = table_size.length
    for (var i = 0 ; i<len ; ++i){

        console.log(table_size[i].relation) 
        table_names[i]  = table_size[i].relation 

        var size = +table_size[i].total_size

        if (control.scale == true){
            if(size!=0)
            {
            size = Math.round(Math.log10(size)* 100) / 100;
            }
        }

        sizes[i] = size ;

    }

    var myChart = echarts.init(document.getElementById("myGraph"))

    //if user selects the bar chart
    if (control.chart == "bar-chart")
    {
        option = {

            title: {
                x: 'center',
                text: 'Database: ' +currentClient.database,
                subtext: 'Size of each table in MB',
            },
            dataZoom: [
                {
                    id: 'dataZoomX',
                    type: 'slider',
                    xAxisIndex: [0],
                    filterMode: 'empty'
                },
            ],
            color: ['#3398DB'],
            tooltip: {
                trigger: 'axis',
                axisPointer: {            
                    type: 'shadow'       
                }
            },
            toolbox: {
                show: true,
                feature: {
                    dataView: {
                        title : "Data view",
                        show: true, 
                        readOnly: false},
                    saveAsImage: {
                        title : "Save as image",
                        show: true}
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    data: table_names,
                    show : false,
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: 'Size of tables',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                position: 'top',
                                formatter: '{b}\n{c}'
                            }
                        }
                    },
                    barWidth: '60%',
                    data: sizes
                }
            ]
        };
    }

    //if user selects pie chart
    if  (control.chart == "pie-chart")
    {
        //first prepare data for pie chart

        var dataPie  = []
        for(var i=0 ; i<table_size.length ;++i)
        {
            dataPie.push({
                value: +table_size[i].total_size,
                name : table_size[i].relation
            })
        }
        option = {

            title: {
                x: 'center',
                text: 'Database: ' + currentClient.database,
                subtext: 'Size of each table in MB\n\n\n',
            },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },
            toolbox: {
                show: true,
                center: ['50%', '75%'],
                feature: {
                    dataView: {
                        title : "Data view",
                        show: true, 
                        readOnly: false},
                    saveAsImage: {
                        title : "Save as image",
                        show: true}
                }
            },
            legend: {
                type: 'scroll',
                orient: 'vertical',
                left: 10,
            },
            series: [
                {
                    name: currentClient.database,
                    type: 'pie',
                    avoidLabelOverlap: false,
                    label: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    },
                    label: {
                        show : true
                    },
                    labelLine: {
                        show: false
                        
                    },
                    data: dataPie
                }
            ]
        };    
    }

    myChart.setOption(option,true)


}
main();