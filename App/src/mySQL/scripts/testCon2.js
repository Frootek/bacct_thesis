
var path = require("path")
const db =  require (path.join(__dirname,"../db/index.js"))



async function main()
{
    const test = `SELECT COLUMN_NAME
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'employees'
    and TABLE_NAME = "employees"`

    const test2 = `SELECT  as col,COUNT(*) as count_values
    FROM employees
    group by first_name
    ORDER BY count_values desc
    LIMIT 10;`


    const relations =`SELECT
    TABLE_NAME,
    REFERENCED_TABLE_NAME
    FROM information_schema.KEY_COLUMN_USAGE
    WHERE CONSTRAINT_SCHEMA = 'employees' AND
    REFERENCED_TABLE_SCHEMA IS NOT NULL AND
    REFERENCED_TABLE_NAME IS NOT NULL AND
    REFERENCED_COLUMN_NAME IS NOT NULL`


    
    var result = await  db.query(relations)

    var links = []

    for(var i=0, len = result.length; i<len ; ++i )
    {
        var onelink = {
            source: result[i].TABLE_NAME,
            target: result[i].REFERENCED_TABLE_NAME
        }
        links.push(onelink)
    }



    console.log(links)
}


main()