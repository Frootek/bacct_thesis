var mysql      = require('../../../node_modules/mysql2');

const currentClient = JSON.parse(window.localStorage.getItem("client"))
const password = window.localStorage.getItem("password")


const pool = mysql.createPool({
    user: currentClient.user,
    host: currentClient.host,
    database: currentClient.database,
    password: password,
    port: currentClient.port,
});


const promisePool = pool.promise();


module.exports = {
  query: function (text) {
      return promisePool.query(text).then(res => res[0])
  }
}