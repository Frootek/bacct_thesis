var echarts = require ("echarts")
var path = require("path")
const db =  require (path.join(__dirname,"../postgreSQL/db/index.js"))


var max_row = 0;
var bubbleMax = 80

const currentClient = JSON.parse(window.localStorage.getItem("client"))

var showTop20 = document.getElementById("show-20");
var showAll = document.getElementById("show-all");
var slider = document.getElementById("bubble-size")
var output_slider = document.getElementById("bubble-max-value")
var submit = document.getElementById("submit-change")
var select_multiple = document.getElementById("ms")
var custom = document.getElementById("custom-pick")
var regex = document.getElementById("regex")

output_slider.innerHTML = slider.value;

slider.oninput = function() {
    output_slider.innerHTML = this.value;
}

submit.addEventListener("click",function (event) {
    bubbleMax  = slider.value;
    main()
})

custom.addEventListener("click",function(event){
    showAll.classList.remove("btn-primary")
    showTop20.classList.remove("btn-primary")

    control.custom = true;
    main()
})


showTop20.addEventListener("click",function (event) {
    if(control.show != "show-20" ||control.custom==true){
        control.custom=false
        showTop20.classList.add("btn-primary")
        showAll.classList.remove("btn-primary")
        control.show = "show-20"
        main()
    }
})

showAll.addEventListener("click",function (event) {
    if(control.show != "show-all" || control.custom==true){
        control.custom=false
        showTop20.classList.remove("btn-primary")
        showAll.classList.add("btn-primary")
        control.show = "show-all"
        main()
    }
})

regex.addEventListener("click",async function(event){
    var name_like  = document.getElementById("regex_search").value
    name_like = name_like.toString().replace(/\*/g, "%")
    console.log(name_like)
    const queryTableNamesLike = {
        name : "table-names-like",
        text : `SELECT relname as "relation", c.reltuples::bigint AS estimate
        FROM   pg_class c
        JOIN   pg_namespace n ON n.oid = c.relnamespace
        WHERE nspname NOT IN ('pg_catalog', 'information_schema')
        AND relname LIKE '${name_like}'
        AND C.relkind <> 'i'
        AND nspname !~ '^pg_toast'
        AND c.reltuples::bigint!=0;`
    }

    let result = await db.query(queryTableNamesLike);

    let len = result.length
    let regex_select
    for (let i = 0 ; i < len ; i++)
    {

        regex_select = result[i].relation+ " " + result[i].estimate
        var oldValue = $('#ms').val() || []; // retrieve old value selected
        oldValue.push(regex_select); // add the new val
        $('#ms').val(oldValue).trigger('change'); // update the select and notify

    }


    showAll.classList.remove("btn-primary")
    showTop20.classList.remove("btn-primary")

    control.custom = true;
    main()
})

var control = {
    show : "show-20",
    custom : false
}

function initialize_ms(options){
    len = options.length
    select_multiple.length=0
    for(let i = 0;i<len;++i){
        var option_element = document.createElement("option")
        option_element.value = options[i].relation+ " " + options[i].estimate
        option_element.text = options[i].relation +  "   " +  options[i].estimate
        select_multiple.add(option_element)      
    }

}


function calculateBubbleSize(rowSize){
    var currentSize =  (rowSize/max_row) * bubbleMax;

    if (currentSize < 5)
    {
        return bubbleMax / 10 ; 
    }
    if ( currentSize <10)
    {
        return bubbleMax / 5;
    }
    return currentSize 

}


//put everything in main function so i can resolve 
async function main(){

    var  result = []
    if(control.custom==false){
        const queryTableNames = {
            name : "table-names",
            text : `SELECT relname as "relation", c.reltuples::bigint AS estimate
            FROM   pg_class c
            JOIN   pg_namespace n ON n.oid = c.relnamespace
            WHERE nspname NOT IN ('pg_catalog', 'information_schema')
            AND C.relkind <> 'i'
            AND nspname !~ '^pg_toast'
            AND c.reltuples::bigint!=0;`
        }
    
        result = await db.query(queryTableNames)
        result.sort((a,b) => +a.estimate >= +b.estimate   ? -1 : 1)

        initialize_ms(result)
    }

    if(control.custom==true){
        values = Array.from(select_multiple.selectedOptions).map(option=>option.value)
        let len = values.length

        for(let i = 0; i < len ;++i){

            value = {
                relation : values[i].split(" ")[0],
                estimate : +values[i].split(" ")[1]
            }
            result.push(value)
        }
    }


    //console.log(result)
    max_row = result[0].estimate
    var result_size = result.length
    var nodes = []

    
    if(control.show=="show-20" && result_size>20){
        result.length=20;
        result_size=20;
    }

    for(var i = 0; i <result_size ; ++i){
        +result[i].estimate
        node = {
            name : result[i].relation,
            value : result[i].estimate,
            symbolSize : calculateBubbleSize(+result[i].estimate)
        }
        nodes.push(node)

    }

    var myChart = echarts.init(document.getElementById("myGraph"))

    option = {
        title: {
            text: 'Database: ' + currentClient.database,
            top: 'bottom',
            left: 'right'
        },
        tooltip: {},
        animationDurationUpdate: 1500,
        animationEasingUpdate: 'quinticInOut',
        series: [
            {
                type: 'graph',
                layout: 'circular',
                roam: true,
                label: {
                    show: true
                },
                circular: {
                    rotateLabel: true
                },
                data: nodes,
                //links: table_relation,
                focusNodeAdjacency: true,
                lineStyle: {
                    opacity: 0.9,
                    width: 1.8,
                    curveness: 0.1
                },
                itemStyle: {
                    size : 1100,
                    borderColor: '#fff',
                    borderWidth: 1,
                    shadowBlur: 10,
                    shadowColor: 'rgba(0, 0, 0, 0.3)'
                },
            }
        ]}

        myChart.setOption(option,true)

}

main();