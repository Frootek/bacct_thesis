var echarts = require ("echarts")
var path = require("path")
const db =  require (path.join(__dirname,"../postgreSQL/db/index.js"))

const currentClient = JSON.parse(window.localStorage.getItem("client"))

var lin = document.getElementById("lin");
var log = document.getElementById("log");

var dark = document.getElementById("dark-theme");
var light = document.getElementById("light-theme");

var showTen = document.getElementById("show-ten");
var showAll = document.getElementById("show-all");

var select_multiple = document.getElementById("ms")
var custom = document.getElementById("custom-pick")

var regex = document.getElementById("regex")

var control = {
    logScale : false,
    colorScheme : "light",
    show : "show-ten",
    custom : false
}
var color_light ={
    background:"#ffff",
    axis : "black",
    item : "#C1232B"
}
var color_dark ={
    background:"#404a59",
    axis : "white",
    item : "#dd4444"
}

//events for buttons

showTen.addEventListener("click",function (event) {
    if(control.show != "show-ten" || control.custom==true){
        showTen.classList.add("btn-primary")
        showAll.classList.remove("btn-primary")
        control.show = "show-ten"
        control.custom=false
        main()
    }
})

showAll.addEventListener("click",function (event) {
    if(control.show != "show-all" || control.custom==true){
        showTen.classList.remove("btn-primary")
        showAll.classList.add("btn-primary")
        control.show = "show-all"
        control.custom=false
        main()
    }
})

lin.addEventListener("click",function (event) {
    if(control.logScale == true){
        lin.classList.toggle("btn-primary")
        log.classList.toggle("btn-primary")
        control.logScale = false;
        main()
    }
})

log.addEventListener("click",function (event) {
    if(control.logScale == false){
        lin.classList.toggle("btn-primary")
        log.classList.toggle("btn-primary")
        control.logScale = true;
        main()
    }
})

dark.addEventListener("click",function (event) {
    if(control.colorScheme == "light"){
        dark.classList.toggle("btn-primary")
        light.classList.toggle("btn-primary")
        control.colorScheme = "dark";
        main()
    }
})

light.addEventListener("click",function (event) {
    if(control.colorScheme == "dark"){
        dark.classList.toggle("btn-primary")
        light.classList.toggle("btn-primary")
        control.colorScheme = "light";
        main()
    }
})

custom.addEventListener("click",function(event){
    showAll.classList.remove("btn-primary")
    showTen.classList.remove("btn-primary")
    control.custom = true;
    main()
})

regex.addEventListener("click",async function(event){
    var name_like  = document.getElementById("regex_search").value
    name_like = name_like.toString().replace(/\*/g, "%")
    console.log(name_like)
    const queryTableNamesLike = {
        name : "table-names-like",
        text : `SELECT relname as "relation", c.reltuples::bigint AS estimate
        FROM   pg_class c
        JOIN   pg_namespace n ON n.oid = c.relnamespace
        WHERE nspname NOT IN ('pg_catalog', 'information_schema')
        AND relname LIKE '${name_like}'
        AND C.relkind <> 'i'
        AND nspname !~ '^pg_toast'
        AND c.reltuples::bigint!=0;`
    }

    let result = await db.query(queryTableNamesLike);

    let len = result.length
    let regex_select
    for (let i = 0 ; i < len ; i++)
    {

        regex_select = result[i].relation+ " " + result[i].estimate
        var oldValue = $('#ms').val() || []; // retrieve old value selected
        oldValue.push(regex_select); // add the new val
        $('#ms').val(oldValue).trigger('change'); // update the select and notify

    }

    showAll.classList.remove("btn-primary")
    showTen.classList.remove("btn-primary")

    control.custom = true;
    main()
})

function initialize_ms(options){
    len = options.length
    select_multiple.length=0
    for(let i = 0;i<len;++i){
        var option_element = document.createElement("option")
        option_element.value = options[i].relation+ " " + options[i].estimate
        option_element.text = options[i].relation +  "   " +  options[i].estimate
        select_multiple.add(option_element)      
    }
}


async function main(){

    var result = []
    if(control.custom==false)
    {
    //get all table names in pg
        var tableRows = {
            text : `SELECT relname as "relation", c.reltuples::bigint AS estimate
            FROM   pg_class c
            JOIN   pg_namespace n ON n.oid = c.relnamespace
            WHERE nspname NOT IN ('pg_catalog', 'information_schema')
            AND C.relkind <> 'i'
            AND nspname !~ '^pg_toast';`
        }

        result = await db.query(tableRows)
        result.sort((a,b) => +a.estimate >= +b.estimate   ? -1 : 1)
        initialize_ms(result)

    }

    if(control.custom==true){
        values = Array.from(select_multiple.selectedOptions).map(option=>option.value)
        let len = values.length
        for(let i = 0; i < len ;++i){
            value = {
                relation : values[i].split(" ")[0],
                estimate : +values[i].split(" ")[1]
            }
            result.push(value)
        }
    }

    var names = [];
    var values = [] ; 
    

    if(control.show == "show-ten" && result.length > 10 && control.custom==false)
    {
        result.length=10;
    }
    var result_length=result.length
    
    for(var i=0;i< result_length ; i++){
        names.push(result[i].relation)

        if(control.logScale==true)
        {
            if(+result[i].estimate!=0)
            {
            result[i].estimate = Math.round(Math.log10(+result[i].estimate)* 100) / 100;
            }
        }

        values.push(result[i].estimate)
    }


    //we got the values now lets create a graph!
    var myChart = echarts.init(document.getElementById("myGraph"))

    var option = {
        title: {
            x: 'center',
            text: 'Database: '+currentClient.database,
            subtext: 'Number of rows in each table',
        },
        dataZoom: [
            {
                id: 'dataZoomX',
                type: 'slider',
                xAxisIndex: [0],
                filterMode: 'empty'
            },
        ],
        tooltip: {
            trigger: 'axis',
            axisPointer: {            
                type: 'shadow'       
            }
        },
        backgroundColor : control.colorScheme=="light" ? color_light.background : color_dark.background,
        toolbox: {
            show: true,
            feature: {
                dataView: {
                    title : "Data view",
                    show: true, 
                    readOnly: false},
                saveAsImage: {
                    title : "Save as image",
                    show: true}
            }
        },
        calculable: true,
        xAxis: [
            {
                type: 'category',
                show: false,
                data: names
            }
        ],
        yAxis: [
            {
                axisLine: {
                    lineStyle:{
                        color: control.colorScheme=="light" ? color_light.axis : color_dark.axis,
                    },
                },
                axisLabel: {
                    color :control.colorScheme=="light" ? color_light.axis : color_dark.axis,
                },
                type: 'value',
                show: true
            }
        ],
        series: [
            {
                name: 'Row count',
                type: 'bar',
                itemStyle: {
                    normal: {
                        color: control.colorScheme=="light" ? color_light.item : color_dark.item,
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{b}\n{c}'
                        }
                    }
                },
                data: values,
                markPoint: {
                    tooltip: {
                        trigger: 'item',
                        backgroundColor: 'rgba(0,0,0,0)',
                    },
                }
            }
        ]
    };
    
    myChart.setOption(option,true)

}

main()