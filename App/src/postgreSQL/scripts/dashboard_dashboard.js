var echarts = require ("echarts")
var path = require("path")
const db =  require (path.join(__dirname,"../postgreSQL/db/index.js"))

var selectDropdown_1 = document.getElementById("sel1")
var vis  = document.getElementById("visualize")

var prev =document.getElementById("prev")
var next =document.getElementById("next")

const dashboard_table_count = 6

var current_table
var counter_offset = 0
var column_length_chck
var visualize_click = false

selectDropdown_1.addEventListener("change", async function(event){

    let table = document.getElementById("sel1").value
    let tableColumns = {
        text : `SELECT column_name
        FROM information_schema.columns
       WHERE table_name = '${table}';`
    }

    let columns = await db.query(tableColumns)

    column_length_chck = columns.length

    if(column_length_chck>dashboard_table_count)
    {
        prev.disabled= true
        next.disabled= false
        next.classList.toggle("btn-primary")
    }
    else{
        prev.disabled= true
        next.disabled= true
    }

})

prev.addEventListener("click", function() {
    --counter_offset
    updateBtns()
    updateGrid()
})
next.addEventListener("click", function() {
    ++counter_offset;
    updateBtns()
    updateGrid()
})

vis.addEventListener("click" ,function (event){
    cleargrid()
    visualize_click=true
    visualize()
})

function updateBtns()
{
    if(counter_offset>=1){
        prev.disabled= false
        prev.classList.add("btn-primary")
    }else{
        prev.disabled= true
        prev.classList.remove("btn-primary")
    }

    if((counter_offset+1)*dashboard_table_count-1<column_length_chck){
        next.disabled= false
        next.classList.add("btn-primary")
    }else{
        next.disabled= true
        next.classList.remove("btn-primary")
    }

}

function updateGrid(){

    cleargrid()
    visualize()
}

function cleargrid(){
    for(let k=0; k<dashboard_table_count; ++k){
        let row = k%2+1
        let column = k%3+1
        let position = document.getElementById(`myGraph_${row}_${column}`)
        let clearChart = echarts.init(position)
        clearChart.clear()
    }
}

async function main()
{
    const queryTableNames = {
        name : "table-names",
        text : `SELECT relname as table_name
        FROM   pg_class c
        JOIN   pg_namespace n ON n.oid = c.relnamespace
        WHERE nspname NOT IN ('pg_catalog', 'information_schema')
        AND C.relkind <> 'i'
        AND nspname !~ '^pg_toast';`
    }

    var table_names = await db.query(queryTableNames)

    table_names_length = table_names.length

    for (var i = 0 ; i < table_names_length ; ++i)
    {
        var optionElement = document.createElement("option")
        optionElement.value = table_names[i].table_name
        optionElement.text = table_names[i].table_name
        selectDropdown_1.add(optionElement)

    }

}

main()

async function  visualize(){

    if(visualize_click==true){
        current_table = document.getElementById("sel1").value
    }
    var tableColumns = {
        text : `SELECT column_name
        FROM information_schema.columns
       WHERE table_name   = '${current_table}';`
    }

    var columns = await db.query(tableColumns)

    var column_length = columns.length
    column_length_chck = column_length

    //reset counter_offset and update prev and next bts
    //if it was clicked by visual btn
    if(visualize_click==true){
        visualize_click=false
        counter_offset=0
        updateBtns()
    }

    var row_position 
    var column_position 

    var x = 1
    var y = 1

    for (var i = counter_offset*dashboard_table_count; i < (counter_offset+1)*dashboard_table_count && i<column_length; ++i)
    {
        //magic numbers , 2 is no_of_rows and 3 is no_of_columns
        if(x>3)
        {
            row_position= 2
        }else{
            row_position = 1
        }
        column_position = y
        createGraph(row_position,column_position,current_table,columns[i].column_name)
        ++x;
        ++y;
        if(y>=4){
            y=1
        }
    }

}

async function createGraph(row_x,column_y,graph_table,col_name)
{
    //console.log(`myGraph_${row_x}_${column_y}`)

    var position = document.getElementById(`myGraph_${row_x}_${column_y}`)

    var myChart = echarts.init(position)

    var query = {
        text : `SELECT ${col_name} as col,COUNT(*) as count_values
        FROM ${graph_table}
        group by ${col_name}
        ORDER BY count_values desc
        LIMIT 10;;`
    }

    var result = await db.query(query)

    var names = []
    var values = [] 
    var len = result.length

    for(var j = 0 ; j < len; ++j)
    {
        names.push(result[j].col)
        values.push(result[j].count_values)
    }

    var option

    if(result[0].count_values != result[len-1].count_values)
    {
        option = {
            title: {
                x: 'center',
                text: col_name
            },
            dataZoom: [
                {
                    id: 'dataZoomX',
                    type: 'slider',
                    xAxisIndex: [0],
                    filterMode: 'empty'
                },
            ],
            tooltip: {
                trigger: 'axis',
                axisPointer: {            
                    type: 'shadow'       
                }
            },
            backgroundColor : "#ffff",
            toolbox: {
                show: true,
                feature: {
                    dataView: {
                        title : "Data view",
                        show: true, 
                        readOnly: false},
                    saveAsImage: {
                        title : "Save as image",
                        show: true}
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    show: false,
                    data: names
                }
            ],
            yAxis: [
                {
                    axisLine: {
                        lineStyle:{
                            color: "black",
                        },
                    },
                    axisLabel: {
                        color :"black",
                    },
                    type: 'value',
                    show: true
                }
            ],
            series: [
                {
                    name: 'Row count',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: "#C1232B",
                            label: {
                                show: true,
                                position: 'top',
                                formatter: '{b}\n{c}'
                            }
                        }
                    },
                    data: values,
                }
            ]
        };
    }
    //display text of possible unique values
    else{
        var tableRows = {
            text : `SELECT relname as "relation", c.reltuples::bigint AS estimate
            FROM   pg_class c
            JOIN   pg_namespace n ON n.oid = c.relnamespace
            WHERE nspname NOT IN ('pg_catalog', 'information_schema')
            AND C.relkind <> 'i'
            AND nspname !~ '^pg_toast';`
        }
        var no_rows = await db.query(tableRows)
    
        console.log(col_name)
        let result = no_rows.filter(obj => {
            return obj.relation == graph_table
          })

        option = {
            title: {
                text: `\n\n Column ${col_name} contains \n\n about ${result[0].estimate} unique values.`,
                subtext: 'If you want to visualize them go to "Relation visualization".',
                left: 'center'
            },
        };
    }
    myChart.setOption(option,true)

}