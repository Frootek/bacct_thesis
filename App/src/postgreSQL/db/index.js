const {Pool}  = require("pg/lib")
const currentClient = JSON.parse(window.localStorage.getItem("client"))
const password = window.localStorage.getItem("password")


const pool = new Pool({
    user: currentClient.user,
    host: currentClient.host,
    database: currentClient.database,
    password: password,
    port: currentClient.port,
});

module.exports = {
    query: function (text) {
        return pool.query(text).then(res => res.rows)
    }
}