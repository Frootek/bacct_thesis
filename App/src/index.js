const electron = require("electron")
const path = require("path")
const BrowserWindow = electron.remote.BrowserWindow;
const ipc = electron.ipcRenderer
const remote = electron.remote

const postgresFormWindow = document.getElementById('postgres')
const mysqlFormWindow = document.getElementById('mysql')

var win

//clear previous seasision storage
localStorage.clear();

//connect to postgreSQL
postgresFormWindow.addEventListener("click", (event)=>{
    const modalPath = path.join('file://', __dirname, '/forms/connectPostgres.html')

    win = new BrowserWindow({ 
        width: 808, 
        height: 325,
        webPreferences: {
            nodeIntegration: true
            },
    
    })
    

    win.setMenu(null);
    win.on('close', () => { win = null })
    win.loadURL(modalPath)
    win.show()
    //win.webContents.openDevTools()
})
mysqlFormWindow.addEventListener("click", (event)=>{
    const modalPath = path.join('file://', __dirname, '/forms/connectMySQL.html')

    win = new BrowserWindow({ 
        width: 808, 
        height: 325,
        webPreferences: {
            nodeIntegration: true
            },
    
    })

    win.setMenu(null);
    win.on('close', () => { win = null })
    win.loadURL(modalPath)
    win.show()
    //win.webContents.openDevTools()
})


ipc.on("connection-success-notify",function(event,client,password){
    const notification = {
        title: 'Connection successful!',
        body: 'connected to ' + client.database  ,
        icon: path.join(__dirname, '../assets/images/postgres_logo.png')
      }
    new window.Notification(notification.title,notification)

    //console.log(JSON.stringify(client))

    //set storage info
    window.localStorage.setItem("client",JSON.stringify(client))
    window.localStorage.setItem("password",password)

    let win = remote.getCurrentWindow();
    win.maximize()
    win.setResizable(false);
    win.center();
    window.location.href  = "./postgreSQL/dashboard_1_relationships.html"

})

ipc.on("connection-success-notify-mysql",function(event,client,password){
    const notification = {
        title: 'Connection successful!',
        body: 'connected to ' + client.database  ,
        icon: path.join(__dirname, '../assets/images/mysql_logo.png')
      }
    new window.Notification(notification.title,notification)


    //set storage info
    window.localStorage.setItem("client",JSON.stringify(client))
    window.localStorage.setItem("password",password)

    let win = remote.getCurrentWindow();
    win.maximize()
    win.setResizable(false)
    win.center()
    window.location.href  = "./mySQL/dashboard_1_relationships.html"

})

