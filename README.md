# DataVisio by Frootek

DataVisio is a cross platform desktop application developed in Electron.js used for visualizing database's metadata.

## Why is DataVisio application useful?

Imagine you are given a database of which you have very little knowledge about. 
In order to get to know this database better you will need to query the database for its metadata (data about data).
For example, you may be interested in how many tables there are, how big they are in terms of their cardinality (number of rows) or their size, how do they relate to each other, what is data distribution for certain column in a particular table, etc.
DataVisio does all this work for you and most importantly, it visualizes the data which it recieves from these SQL queries.
Data visualizations make big and small data easier for the human brain to understand, and visualization also makes it easier to detect patterns, trends, and outliers in groups of data.

## Installation

To get DataVisio up and running you will need to clone this repository and running following commands inside App folder.

```bash
npm install # this might take a while
npm start
```

## Usage

Choose between supported databases (currently only postgresql and mysql databases are supported) and type in connection details.
For this demonstration I am using  sample mysql database  ["employees"](https://dev.mysql.com/doc/employee/en/).

![Image1](images/image_1.JPG)

![Image2](images/image_2.JPG)


Upon entering valid connection details you can start viewing database visualization.

Here are few examples.

![Image3](images/image_3.JPG)

![Image4](images/image_4.JPG)

![Image5](images/image_5.JPG)

![Image6](images/image_6.JPG)


When we see these visualizations we immediately get a better grasp on  how this database is structured and how data in these tables are distributed
(for example, we can see that there are more male employees and most common title is "engineer").


## Used frameworks and libraries

- [Electron.js](https://www.electronjs.org/)
- [Echarts](https://echarts.apache.org/en/)


## Project status, support and contributing

I created this project while working on my bachelor's thesis in computer engineering and have since shifted my focus on other projects and interests.
However, if you need support or wish to contribute to this project feel free to contact me on my personal email marin.juric@hotmail.com.

## License
This software is licensed under the [MIT License](LICENSE).